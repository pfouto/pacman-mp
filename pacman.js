var legion;
var objectStore = null;
var messageAPI = null;
var initDone = false;

var DIRECTIONS = {
    NONE: 4,
    UP: 3,
    LEFT: 2,
    DOWN: 1,
    RIGHT: 11
};

var Pacman = {
    WALL: 0,
    BISCUIT: 1,
    EMPTY: 2,
    BLUE_PILL: 4,
    RED_PILL: 5,
    FPS: 30
};

var STATE = {
    WAITING: 0,
    COUNTDOWN: 1,
    PLAYING: 2,
    PAUSE: 3,
    DYING: 4,
    FINISHED: 5
};

var INITVALUES = {
    pacman: {
        position: {"x": 9, "y": 12},
        lives: 3,
        biscuitsEaten: 0,
        score: 0,
        direction: DIRECTIONS.LEFT,
        due: DIRECTIONS.LEFT
    },
    ghosts: [
        {
            position: {"x": 1, "y": 1},
            direction: DIRECTIONS.LEFT,
            due: DIRECTIONS.LEFT,
            colour: "#00FFDE"
        }, {
            position: {"x": 17, "y": 20},
            direction: DIRECTIONS.RIGHT,
            due: DIRECTIONS.LEFT,
            colour: "#FF0000"
        }, {
            position: {"x": 1, "y": 20},
            direction: DIRECTIONS.LEFT,
            due: DIRECTIONS.LEFT,
            colour: "#FFB8DE"
        }, {
            position: {"x": 17, "y": 1},
            direction: DIRECTIONS.RIGHT,
            due: DIRECTIONS.LEFT,
            colour: "#FFB847"
        }
    ],

    map: [
        [0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 4, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 4, 0],
        [0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0],
        [0, 1, 1, 1, 5, 0, 1, 1, 1, 0, 1, 1, 1, 0, 5, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 2, 2, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 2, 2, 0],
        [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
        [2, 2, 2, 2, 1, 1, 1, 0, 2, 2, 2, 0, 1, 1, 1, 2, 2, 2, 2],
        [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
        [0, 2, 2, 0, 1, 0, 1, 1, 1, 2, 1, 1, 1, 0, 1, 0, 2, 2, 0],
        [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
        [0, 1, 1, 1, 5, 1, 1, 1, 1, 0, 1, 1, 1, 1, 5, 1, 1, 1, 0],
        [0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
        [0, 4, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 4, 0],
        [0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0],
        [0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0]
    ]
};

var INITVALUES2 = {
    pacman: {
        position: {"x": 1, "y": 8},
        lives: 3,
        biscuitsEaten: 0,
        score: 0,
        direction: DIRECTIONS.LEFT,
        due: DIRECTIONS.LEFT
    },
    ghosts: [
        {
            position: {"x": 1, "y": 1},
            direction: DIRECTIONS.LEFT,
            due: DIRECTIONS.LEFT,
            colour: "#00FFDE"
        }, {
            position: {"x": 1, "y": 1},
            direction: DIRECTIONS.RIGHT,
            due: DIRECTIONS.LEFT,
            colour: "#FF0000"
        }, {
            position: {"x": 1, "y": 1},
            direction: DIRECTIONS.LEFT,
            due: DIRECTIONS.LEFT,
            colour: "#FFB8DE"
        }, {
            position: {"x": 1, "y": 1},
            direction: DIRECTIONS.RIGHT,
            due: DIRECTIONS.LEFT,
            colour: "#FFB847"
        }
    ],

    map: [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
};

var spritesMapping = [
    {
        l: true, r: true, u: true, d: true,
        p: 7
    }, {
        l: true, r: true, u: true, d: false,
        p: 5
    }, {
        l: true, r: true, u: false, d: true,
        p: 13
    }, {
        l: true, r: true, u: false, d: false,
        p: 2
    }, {
        l: true, r: false, u: true, d: true,
        p: 6
    }, {
        l: true, r: false, u: true, d: false,
        p: 9
    }, {
        l: true, r: false, u: false, d: true,
        p: 1
    }, {
        l: true, r: false, u: false, d: false,
        p: 12
    }, {
        l: false, r: true, u: true, d: true,
        p: 14
    }, {
        l: false, r: true, u: true, d: false,
        p: 8
    }, {
        l: false, r: true, u: false, d: true,
        p: 0
    }, {
        l: false, r: true, u: false, d: false,
        p: 4
    }, {
        l: false, r: false, u: true, d: true,
        p: 10
    }, {
        l: false, r: false, u: true, d: false,
        p: 3
    }, {
        l: false, r: false, u: false, d: true,
        p: 11
    }, {
        l: false, r: false, u: false, d: false,
        p: 15
    }
];

var myId,
    localPacman, sharedPacmanMap,
    localGhosts = [], sharedGhostsMap = [],
    playersMap,
    gamestateMap,
    gameMapInfoMap,
    gameMapList,
    map,
    controlling = null,
    wrapper,
    tick = 0,
    timerStart = null,
    ctx = null,
    timer = null,
    biscuitLog = [],
    timestampLog = [],
    movementLog = [],
    collisionEnabled = true,
    soundEnabled = false;

var img = document.getElementById("level");

var KEY = {
    'BACKSPACE': 8,
    'TAB': 9,
    'NUM_PAD_CLEAR': 12,
    'ENTER': 13,
    'SHIFT': 16,
    'CTRL': 17,
    'ALT': 18,
    'PAUSE': 19,
    'CAPS_LOCK': 20,
    'ESCAPE': 27,
    'SPACEBAR': 32,
    'PAGE_UP': 33,
    'PAGE_DOWN': 34,
    'END': 35,
    'HOME': 36,
    'ARROW_LEFT': 37,
    'ARROW_UP': 38,
    'ARROW_RIGHT': 39,
    'ARROW_DOWN': 40,
    'PRINT_SCREEN': 44,
    'INSERT': 45,
    'DELETE': 46,
    'SEMICOLON': 59,
    'WINDOWS_LEFT': 91,
    'WINDOWS_RIGHT': 92,
    'SELECT': 93,
    'NUM_PAD_ASTERISK': 106,
    'NUM_PAD_PLUS_SIGN': 107,
    'NUM_PAD_HYPHEN-MINUS': 109,
    'NUM_PAD_FULL_STOP': 110,
    'NUM_PAD_SOLIDUS': 111,
    'NUM_LOCK': 144,
    'SCROLL_LOCK': 145,
    'SEMICOLON2': 186,
    'EQUALS_SIGN': 187,
    'COMMA': 188,
    'HYPHEN-MINUS': 189,
    'FULL_STOP': 190,
    'SOLIDUS': 191,
    'GRAVE_ACCENT': 192,
    'LEFT_SQUARE_BRACKET': 219,
    'REVERSE_SOLIDUS': 220,
    'RIGHT_SQUARE_BRACKET': 221,
    'APOSTROPHE': 222
};

(function () {
    /* 0 - 9 */
    for (var i = 48; i <= 57; i++) {
        KEY['' + (i - 48)] = i;
    }
    /* A - Z */
    for (i = 65; i <= 90; i++) {
        KEY['' + String.fromCharCode(i)] = i;
    }
    /* NUM_PAD_0 - NUM_PAD_9 */
    for (i = 96; i <= 105; i++) {
        KEY['NUM_PAD_' + (i - 96)] = i;
    }
    /* F1 - F12 */
    for (i = 112; i <= 123; i++) {
        KEY['F' + (i - 112 + 1)] = i;
    }
})();

var keyMap = {};
keyMap[KEY.ARROW_LEFT] = DIRECTIONS.LEFT;
keyMap[KEY.ARROW_UP] = DIRECTIONS.UP;
keyMap[KEY.ARROW_RIGHT] = DIRECTIONS.RIGHT;
keyMap[KEY.ARROW_DOWN] = DIRECTIONS.DOWN;


function Ghost(id, game, map, colour, position, direction, eatable, eaten, due) {

    this.id = id;
    this.game = game;
    this.map = map;
    this.colour = colour;
    this.position = position;
    this.direction = direction;
    this.eatable = eatable;
    this.eatableStartTick = 0;
    this.eaten = eaten;
    this.eatenStartTick = 0;
    this.due = due;
    this.lastRandTurnTick = 0;

    this.getNewCoord = function (dir, current) {

        var speed = this.eatable ? 0.5 : this.eaten ? 2 : 1;
        speed = speed * (1 + localPacman.biscuitsEaten / 182);
        var xSpeed = (dir === DIRECTIONS.LEFT && -speed || dir === DIRECTIONS.RIGHT && speed || 0),
            ySpeed = (dir === DIRECTIONS.DOWN && speed || dir === DIRECTIONS.UP && -speed || 0);

        return {
            "x": this.addBounded(current.x, xSpeed),
            "y": this.addBounded(current.y, ySpeed)
        };
    };

    /* Collision detection(walls) is done when a ghost lands on an
     * exact block, make sure they dont skip over it
     */
    this.addBounded = function (x1, x2) {
        var rem = x1 % 10,
            result = rem + x2;
        if (rem !== 0 && result > 10) {
            return x1 + (10 - rem);
        } else if (rem > 0 && result < 0) {
            return x1 - rem;
        }
        return x1 + x2;
    };

    this.isDangerous = function () {
        return this.eaten === false && this.eatable === false;
    };

    this.getAlternateDirections = function () {
        var dirs = (this.direction === DIRECTIONS.LEFT || this.direction === DIRECTIONS.RIGHT) ? [DIRECTIONS.UP, DIRECTIONS.DOWN] : [DIRECTIONS.LEFT, DIRECTIONS.RIGHT];
        shuffle(dirs);
        return dirs;
    };

    this.isOppositeDir = function (dir1, dir2) {
        if (dir1 === DIRECTIONS.LEFT)
            return (dir2 === DIRECTIONS.RIGHT);
        if (dir1 === DIRECTIONS.RIGHT)
            return (dir2 === DIRECTIONS.LEFT);
        if (dir1 === DIRECTIONS.UP)
            return (dir2 === DIRECTIONS.DOWN);
        if (dir1 === DIRECTIONS.DOWN)
            return (dir2 === DIRECTIONS.UP);
    };

    this.getRandomDirection = function () {
        var moves = (this.direction === DIRECTIONS.LEFT || this.direction === DIRECTIONS.RIGHT) ? [DIRECTIONS.UP, DIRECTIONS.DOWN] : [DIRECTIONS.LEFT, DIRECTIONS.RIGHT];
        return moves[Math.floor(Math.random() * 2)];
    };
    /*
     this.getRandomDirectionIncludingFront = function() {
     var moves = (this.direction === DIRECTIONS.LEFT || this.direction === DIRECTIONS.RIGHT) ?
     [DIRECTIONS.UP, DIRECTIONS.DOWN, this.direction] : [DIRECTIONS.LEFT, DIRECTIONS.RIGHT, this.direction];
     return moves[Math.floor(Math.random() * 3)];
     };*/

    this.onWholeSquare = function (x) {
        return x % 10 === 0;
    };
    /*
     this.oppositeDirection = function(dir) {
     return dir === DIRECTIONS.LEFT && DIRECTIONS.RIGHT ||
     dir === DIRECTIONS.RIGHT && DIRECTIONS.LEFT ||
     dir === DIRECTIONS.UP && DIRECTIONS.DOWN || DIRECTIONS.UP;
     };
     */
    this.pointToCoord = function (x) {
        return Math.round(x / 10);
    };

    this.nextSquare = function (x, dir) {
        var rem = x % 10;
        if (rem === 0) {
            return x;
        } else if (dir === DIRECTIONS.RIGHT || dir === DIRECTIONS.DOWN) {
            return x + (10 - rem);
        } else {
            return x - rem;
        }
    };

    this.onGridSquare = function (pos) {
        return this.onWholeSquare(pos.y) && this.onWholeSquare(pos.x);
    };

    this.getColour = function () {
        if (this.eatable) {
            if (this.game.secondsAgo(this.eatableStartTick) > 5) {
                return game.getTick() % 20 > 10 ? "#FFFFFF" : "#0000BB";
            } else {
                return "#0000BB";
            }
        } else if (this.eaten) {
            return "#222";
        }
        return this.colour;
    };

    this.draw = function (ctx) {

        var s = map.blockSize,
            top = (this.position.y / 10) * s,
            left = (this.position.x / 10) * s;

        var tl = left + s;
        var base = top + s - 3;
        var inc = s / 10;

        var high = game.getTick() % 10 > 5 ? 3 : -3;
        var low = game.getTick() % 10 > 5 ? -3 : 3;

        ctx.fillStyle = this.getColour();
        ctx.beginPath();

        ctx.moveTo(left, base);

        ctx.quadraticCurveTo(left, top, left + (s / 2), top);
        ctx.quadraticCurveTo(left + s, top, left + s, base);

        // Wavy things at the bottom
        ctx.quadraticCurveTo(tl - (inc), base + high, tl - (inc * 2), base);
        ctx.quadraticCurveTo(tl - (inc * 3), base + low, tl - (inc * 4), base);
        ctx.quadraticCurveTo(tl - (inc * 5), base + high, tl - (inc * 6), base);
        ctx.quadraticCurveTo(tl - (inc * 7), base + low, tl - (inc * 8), base);
        ctx.quadraticCurveTo(tl - (inc * 9), base + high, tl - (inc * 10), base);

        ctx.closePath();
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = "#FFF";
        ctx.arc(left + 6, top + 6, s / 6, 0, 300, false);
        ctx.arc((left + s) - 6, top + 6, s / 6, 0, 300, false);
        ctx.closePath();
        ctx.fill();

        var f = s / 12;
        var off = {};
        off[DIRECTIONS.RIGHT] = [f, 0];
        off[DIRECTIONS.LEFT] = [-f, 0];
        off[DIRECTIONS.UP] = [0, -f];
        off[DIRECTIONS.DOWN] = [0, f];

        ctx.beginPath();
        ctx.fillStyle = "#000";
        ctx.arc(left + 6 + off[this.direction][0], top + 6 + off[this.direction][1],
            s / 15, 0, 300, false);
        ctx.arc((left + s) - 6 + off[this.direction][0], top + 6 + off[this.direction][1],
            s / 15, 0, 300, false);
        ctx.closePath();
        ctx.fill();

    };

    this.move = function () {

        var oldPos = this.position,
            onGrid = this.onGridSquare(this.position),
            npos = null,
            sendPos = false,
            sendDue = false,
            tpos;

        if (!onGrid) {
            npos = this.getNewCoord(this.direction, this.position);
        } else {

            tpos = this.getNewCoord(this.direction, this.position);

            if (this.game.isPacman() && onGrid && (this.game.getTick() - this.lastRandTurnTick > 20) &&
                (!this.game.isPlayerControlled(this.id) || localGhosts[this.id].eaten)) {
                this.due = this.getRandomDirection();
                this.lastRandTurnTick = this.game.getTick();
                sendDue = true;
            }

            //Check if can turn
            if ((this.due !== this.direction) && !this.isOppositeDir(this.due, this.direction)) {
                var dpos = this.getNewCoord(this.due, this.position);
                if (map.isFloorSpace({
                        "y": this.pointToCoord(this.nextSquare(dpos.y, this.due)),
                        "x": this.pointToCoord(this.nextSquare(dpos.x, this.due))
                    })) {

                    this.direction = this.due;
                    sendPos = true;
                    npos = dpos;
                } else {
                }
            }
            //If didnt turn and got a wall
            if (npos === null && this.game.isPacman() && map.isWallSpace({
                    "y": this.pointToCoord(this.nextSquare(tpos.y, this.direction)),
                    "x": this.pointToCoord(this.nextSquare(tpos.x, this.direction))
                })) {

                var alternateDirections = this.getAlternateDirections();
                //Try turning one way
                tpos = this.getNewCoord(alternateDirections[0], this.position);
                if (map.isFloorSpace({
                        "y": this.pointToCoord(this.nextSquare(tpos.y, alternateDirections[0])),
                        "x": this.pointToCoord(this.nextSquare(tpos.x, alternateDirections[0]))
                    })) {
                    this.direction = this.due = alternateDirections[0];
                    sendDue = true;
                    sendPos = true;
                    npos = tpos;
                }
                //Try the other
                if (npos === null) {
                    tpos = this.getNewCoord(alternateDirections[1], this.position);
                    if (map.isFloorSpace({
                            "y": this.pointToCoord(this.nextSquare(tpos.y, alternateDirections[1])),
                            "x": this.pointToCoord(this.nextSquare(tpos.x, alternateDirections[1]))
                        })) {
                        this.direction = this.due = alternateDirections[1];
                        sendDue = true;
                        sendPos = true;
                        npos = tpos;
                    }
                }
            }
            tpos = this.getNewCoord(this.direction, this.position);
            if (npos === null && !map.isWallSpace({
                    "y": this.pointToCoord(this.nextSquare(tpos.y, this.direction)),
                    "x": this.pointToCoord(this.nextSquare(tpos.x, this.direction))
                })) {
                npos = tpos;
            } else {
                npos = oldPos;
            }
        }
        //Warps
        var tmp = map.pane(npos, this.direction);
        if (tmp) {
            npos = tmp;
        }
        //update and return
        this.position = npos;
        return {
            "new": this.position,
            "old": oldPos,
            "sendPos": sendPos,
            "sendDue": sendDue
        };
    };
}

function Pac(map, position, lives, score, direction, due, biscuitsEaten) {

    this.map = map;
    this.position = position;
    this.lives = lives;
    this.biscuitsEaten = biscuitsEaten;
    this.score = score;
    this.direction = direction;
    this.due = due;

    this.getNewCoord = function (dir, current) {
        return {
            "x": current.x + (dir === DIRECTIONS.LEFT && -2 || dir === DIRECTIONS.RIGHT && 2 || 0),
            "y": current.y + (dir === DIRECTIONS.DOWN && 2 || dir === DIRECTIONS.UP && -2 || 0)
        };
    };

    this.onWholeSquare = function (x) {
        return x % 10 === 0;
    };

    this.pointToCoord = function (x) {
        return Math.round(x / 10);
    };

    this.nextSquare = function (x, dir) {
        var rem = x % 10;
        if (rem === 0) {
            return x;
        } else if (dir === DIRECTIONS.RIGHT || dir === DIRECTIONS.DOWN) {
            return x + (10 - rem);
        } else {
            return x - rem;
        }
    };

    this.next = function (pos, dir) {
        return {
            "y": this.pointToCoord(this.nextSquare(pos.y, dir)),
            "x": this.pointToCoord(this.nextSquare(pos.x, dir))
        };
    };

    this.onGridSquare = function (pos) {
        return this.onWholeSquare(pos.y) && this.onWholeSquare(pos.x);
    };

    this.isOnSamePlane = function (due, dir) {
        return ((due === DIRECTIONS.LEFT || due === DIRECTIONS.RIGHT) &&
            (dir === DIRECTIONS.LEFT || dir === DIRECTIONS.RIGHT)) ||
            ((due === DIRECTIONS.UP || due === DIRECTIONS.DOWN) &&
            (dir === DIRECTIONS.UP || dir === DIRECTIONS.DOWN));
    };

    this.move = function () {

        var npos = null,
            nextWhole,
            oldPosition = this.position,
            sendPos = false;

        //check if can turn
        if (this.due !== this.direction) {
            npos = this.getNewCoord(this.due, this.position);

            if (this.isOnSamePlane(this.due, this.direction) ||
                (this.onGridSquare(this.position) && map.isFloorSpace(this.next(npos, this.due)))) {
                this.direction = this.due;
                sendPos = true;
            } else {
                npos = null;
            }
        }

        //didnt turn, keep going front
        if (npos === null) {
            npos = this.getNewCoord(this.direction, this.position);
        }

        //end of road, stop
        if (this.onGridSquare(this.position) && map.isWallSpace(this.next(npos, this.direction))) {
            this.direction = DIRECTIONS.NONE; //STOPPED
        }


        //stopped, return
        if (this.direction === DIRECTIONS.NONE) {
            return {
                "new": this.position,
                "old": this.position,
                "nextWhole": null,
                "sendPos": sendPos
            };
        }

        //check "portals"

        var tmp = map.pane(npos, this.direction);
        if (tmp) {
            npos = tmp;
        }


        this.position = npos;
        nextWhole = this.next(this.position, this.direction);
        return {
            "new": this.position,
            "old": oldPosition,
            "nextWhole": nextWhole,
            "sendPos": sendPos
        };
    };

    this.isMidSquare = function (x) {
        var rem = x % 10;
        return rem > 3 || rem < 7;
    };

    this.calcAngle = function (dir, pos) {
        if (dir === DIRECTIONS.RIGHT && (pos.x % 10 < 5)) {
            return {
                "start": 0.25,
                "end": 1.75,
                "direction": false
            };
        } else if (dir === DIRECTIONS.DOWN && (pos.y % 10 < 5)) {
            return {
                "start": 0.75,
                "end": 2.25,
                "direction": false
            };
        } else if (dir === DIRECTIONS.UP && (pos.y % 10 < 5)) {
            return {
                "start": 1.25,
                "end": 1.75,
                "direction": true
            };
        } else if (dir === DIRECTIONS.LEFT && (pos.x % 10 < 5)) {
            return {
                "start": 0.75,
                "end": 1.25,
                "direction": true
            };
        }
        return {
            "start": 0,
            "end": 2,
            "direction": false
        };
    };

    this.drawDead = function (ctx, amount) {

        var size = map.blockSize,
            half = size / 2;

        if (amount >= 1) {
            return;
        }

        ctx.fillStyle = "#FFFF00";
        ctx.beginPath();
        ctx.moveTo(((this.position.x / 10) * size) + half, ((this.position.y / 10) * size) + half);

        ctx.arc(((this.position.x / 10) * size) + half, ((this.position.y / 10) * size) + half,
            half, 0, Math.PI * 2 * amount, true);

        ctx.fill();
    };

    this.draw = function (ctx) {

        var s = map.blockSize,
            angle = this.calcAngle(this.direction, this.position);

        ctx.fillStyle = "#FFFF00";

        ctx.beginPath();

        ctx.moveTo(((this.position.x / 10) * s) + s / 2, ((this.position.y / 10) * s) + s / 2);

        ctx.arc(((this.position.x / 10) * s) + s / 2, ((this.position.y / 10) * s) + s / 2,
            s / 2, Math.PI * angle.start,
            Math.PI * angle.end, angle.direction);

        ctx.fill();
    };
}

function GameMap(sharedList, mapHeight, mapWidth, mapBlockSize) {
    this.list = sharedList;
    this.spriteMap = {};
    this.blockSize = mapBlockSize;
    this.height = mapHeight;
    this.width = mapWidth;
    this.pillSize = 0;

    this.pane = function (pos, dir) {

        if (pos.x >= this.width * 10 && dir === DIRECTIONS.RIGHT) {
            return {
                "y": pos.y,
                "x": -1 * 10
            };
        }

        if (pos.x <= -1 * 10 && dir === DIRECTIONS.LEFT) {
            return {
                "y": pos.y,
                "x": this.width * 10
            };
        }

        if (pos.y >= this.height * 10 && dir === DIRECTIONS.DOWN) {
            return {
                "y": -1 * 10,
                "x": pos.x
            };
        }

        if (pos.y <= -1 * 10 && dir === DIRECTIONS.UP) {
            return {
                "y": this.height * 10,
                "x": pos.x
            };
        }

        return false;
    };

    this.withinBounds = function (y, x) {
        return y >= 0 && y < this.height && x >= 0 && x < this.width;
    };

    this.isWallSpace = function (pos) {
        return this.withinBounds(pos.y, pos.x) && this.blockByPos(pos) === Pacman.WALL;
    };

    this.isFloorSpace = function (pos) {
        if (!this.withinBounds(pos.y, pos.x)) {
            return false;
        }
        var piece = this.blockByPos(pos);
        return piece !== Pacman.WALL;
    };

    this.blockByPos = function (pos) {
        return this.blockByCoords(pos.y, pos.x);
    };

    this.blockByCoords = function (y, x) {
        return parseInt(this.list.get(this.width * y + x));
    };

    this.setBlock = function (pos, type) {
        this.list.set(this.width * pos.y + pos.x, type);
    };

    this.draw = function (ctx) {
        var size = this.blockSize;
        ctx.fillStyle = "#000";
        ctx.fillRect(0, 0, this.width * size, this.height * size);
        //this.drawWalls(ctx);
        this.drawBlocks(ctx);
    };

    this.drawBlocks = function (ctx) {
        for (var y = 0; y < this.height; y += 1) {
            for (var x = 0; x < this.width; x += 1) {
                var layout = this.blockByCoords(y, x);

                ctx.beginPath();

                //TODO switch
                if (layout === Pacman.EMPTY || layout === Pacman.BISCUIT || layout === Pacman.RED_PILL || layout === Pacman.BLUE_PILL) {
                    ctx.fillStyle = "#000";
                    ctx.fillRect((x * this.blockSize), (y * this.blockSize), this.blockSize, this.blockSize);
                    if (layout === Pacman.BISCUIT) {
                        ctx.fillStyle = "#FFF";
                        ctx.fillRect((x * this.blockSize) + (this.blockSize / 2.5), (y * this.blockSize) + (this.blockSize / 2.5),
                            this.blockSize / 6, this.blockSize / 6);
                    } else if (layout === Pacman.BLUE_PILL) {

                        ctx.fillStyle = "#1b42ff";
                        ctx.arc((x * this.blockSize) + this.blockSize / 2, (y * this.blockSize) + this.blockSize / 2,
                            Math.abs(5 - (this.pillSize / 3)),
                            0,
                            Math.PI * 2, false);
                        ctx.fill();
                    } else if (layout === Pacman.RED_PILL) {

                        ctx.fillStyle = "#ff2f2a";
                        ctx.arc((x * this.blockSize) + this.blockSize / 2, (y * this.blockSize) + this.blockSize / 2,
                            Math.abs(5 - (this.pillSize / 3)),
                            0,
                            Math.PI * 2, false);
                        ctx.fill();
                    }
                }
                if (layout === Pacman.WALL && ((this.width * y + x) in this.spriteMap)) {
                    ctx.drawImage(img, this.spriteMap[this.width * y + x] * 30, 0, 30, 30,
                        x * this.blockSize, y * this.blockSize, this.blockSize, this.blockSize);
                }
                ctx.closePath();
            }
        }
    };

    this.buildSpriteMap = function () {
        for (var y = 0; y < this.height; y += 1) {
            for (var x = 0; x < this.width; x += 1) {
                var layout = this.blockByCoords(y, x);
                if (layout === Pacman.WALL) {
                    var l = (x !== 0 && this.blockByCoords(y, x - 1) === Pacman.WALL),
                        r = ((x !== (this.width - 1)) && this.blockByCoords(y, x + 1) === Pacman.WALL),
                        u = (y !== 0 && this.blockByCoords(y - 1, x) === Pacman.WALL),
                        d = ((y !== (this.height - 1)) && this.blockByCoords(y + 1, x) === Pacman.WALL);

                    for (var i = 0; i < spritesMapping.length; i++) {
                        if (spritesMapping[i].u === u &&
                            spritesMapping[i].d === d &&
                            spritesMapping[i].l === l &&
                            spritesMapping[i].r === r) {
                            this.spriteMap[this.width * y + x] = spritesMapping[i].p;
                            break;
                        }
                    }
                }
            }
        }
    }
}

function Game() {

    myId = guid();

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    //SHARED

    function isPacman() {
        return controlling === 'pacman';
    }

    function isPlayerControlled(id) {
        return playersMap.contains('ghost' + id);
    }

    function getTick() {
        return tick;
    }

    function secondsAgo(starttick) {
        return (getTick() - starttick) / Pacman.FPS;
    }

    function setState(nState) {
        gamestateMap.set('state', nState);
    }

    function getState() {
        return parseInt(gamestateMap.get('state')[0]);
    }

    function update() {
        if (getState() !== STATE.PAUSE) {
            tick++;
        }
        switch (getState()) {
            case STATE.PLAYING:
                var ghostPos = [];
                for (i = 0; i < localGhosts.length; i += 1) {
                    ghostPos.push(localGhosts[i].move());
                }
                var moveRet = localPacman.move();

                if (isPacman()) { //Pacman only update
                    if (moveRet["sendPos"]) {
                        sharedPacmanMap.set('direction', localPacman.direction);
                        sharedPacmanMap.set('position', localPacman.position);
                    }
                    //Check pills/bisbuits
                    var pacPos = moveRet["new"];
                    if (moveRet["nextWhole"]) {
                        var nextWhole = moveRet["nextWhole"];
                        var block = map.blockByPos(nextWhole);
                        if ((localPacman.isMidSquare(pacPos.y) || localPacman.isMidSquare(pacPos.x)) &&
                            block === Pacman.BISCUIT || block === Pacman.BLUE_PILL || block === Pacman.RED_PILL) {
                            //remove block
                            map.setBlock(nextWhole, Pacman.EMPTY);
                            //set score
                            sharedPacmanMap.set('score',
                                localPacman.score + ((block === Pacman.BISCUIT) ? 10 : 50));
                            localPacman.biscuitsEaten += 1;
                            sharedPacmanMap.set('biscuitsEaten', localPacman.biscuitsEaten);
                            if (localPacman.biscuitsEaten === 182) {
                                setState(STATE.FINISHED);
                            }
                            if (block === Pacman.BLUE_PILL) {
                                for (var j = 0; j < localGhosts.length; j += 1) {
                                    sharedGhostsMap[j].set('eatable', false);
                                    sharedGhostsMap[j].set('eatable', true);
                                }
                            }
                            if (block === Pacman.RED_PILL) {
                                var tempPosStore = [{}, {}, {}, {}];
                                for (var k = 0; k < 4; k++) {
                                    tempPosStore[k].position = localGhosts[(k + 1) % 4].position;
                                    tempPosStore[k].direction = localGhosts[(k + 1) % 4].direction;
                                    tempPosStore[k].due = localGhosts[(k + 1) % 4].due;
                                }
                                for (var z = 0; z < 4; z++) {
                                    sharedGhostsMap[z].set('position',
                                        {
                                            x: Math.round(tempPosStore[z].position.x),
                                            y: Math.round(tempPosStore[z].position.y)
                                        });
                                    sharedGhostsMap[z].set('direction', tempPosStore[z].direction);
                                    sharedGhostsMap[z].set('due', tempPosStore[z].due);

                                    /*localGhosts[z].position =
                                     {
                                     x: Math.round(tempPosStore[z].position.x),
                                     y: Math.round(tempPosStore[z].position.y)
                                     };
                                     localGhosts[z].direction = tempPosStore[z].direction;
                                     localGhosts[z].due = tempPosStore[z].due;*/
                                }
                            }
                        }
                    }

                    //update ghosts
                    for (var i = 0; i < localGhosts.length; i += 1) {
                        var eatable = localGhosts[i].eatable;
                        var eaten = localGhosts[i].eaten;
                        var eatableStartTick = localGhosts[i].eatableStartTick;
                        var eatenStartTick = localGhosts[i].eatenStartTick;
                        if (eatable && secondsAgo(eatableStartTick) > 8) {
                            sharedGhostsMap[i].set('eatable', false);
                        }
                        if (eaten && secondsAgo(eatenStartTick) > 3) {
                            sharedGhostsMap[i].set('eaten', false);
                        }
                        if (ghostPos[i]["sendPos"]) {
                            sharedGhostsMap[i].set('direction', localGhosts[i].direction);
                            sharedGhostsMap[i].set('position', localGhosts[i].position);
                        }
                        if (ghostPos[i]["sendDue"]) {
                            sharedGhostsMap[i].set('due', localGhosts[i].due);
                        }

                        //Check collisions
                        if (collided(pacPos, ghostPos[i]["new"]) && collisionEnabled) {
                            if (localGhosts[i].eatable) {
                                sharedGhostsMap[i].set('eatable', false);
                                sharedGhostsMap[i].set('eaten', true);
                                sharedPacmanMap.set('score', localPacman.score + 50);
                            } else if (localGhosts[i].isDangerous()) {
                                setState(STATE.DYING);
                                timerStart = tick;
                            }
                        }
                    }
                }
                break;
            case STATE.DYING:
                if (isPacman()) {
                    if (secondsAgo(timerStart) > 2) {
                        loseLife();
                    }
                }
                break;
            case STATE.COUNTDOWN:
                if (isPacman()) {
                    if (secondsAgo(timerStart) > 5) {
                        setState(STATE.PLAYING);
                    }
                }
                break;
        }
    }

    function mainLoop() {
        update();
        draw();
    }

    function restartPositions() {
        sharedPacmanMap.set('position',
            {x: INITVALUES.pacman.position.x * 10, y: INITVALUES.pacman.position.y * 10});
        sharedPacmanMap.set('direction', INITVALUES.pacman.direction);
        sharedPacmanMap.set('due', INITVALUES.pacman.due);
        for (var i = 0; i < localGhosts.length; i++) {
            sharedGhostsMap[i].set('position',
                {x: INITVALUES.ghosts[i].position.x * 10, y: INITVALUES.ghosts[i].position.y * 10});
            sharedGhostsMap[i].set('direction', INITVALUES.ghosts[i].direction);
            sharedGhostsMap[i].set('due', INITVALUES.ghosts[i].due);
        }
    }

    function collided(pac, ghost) {
        return (Math.sqrt(Math.pow(ghost.x - pac.x, 2) +
                Math.pow(ghost.y - pac.y, 2))) < 10;
    }

    function pause() {
        if (getState() === STATE.PAUSE)
            return;
        gamestateMap.set('prevstate', getState());
        setState(STATE.PAUSE);
    }

    function resume() {
        if (getState() !== STATE.PAUSE)
            return;
        setState(parseInt(gamestateMap.get('prevstate')[0]));
    }

    function loseLife() {
        localPacman.lives -= 1;
        sharedPacmanMap.set('lives', localPacman.lives);
        if (localPacman.lives <= 0) {
            setState(STATE.FINISHED);
        } else {
            restartPositions();
            setState(STATE.COUNTDOWN);
        }
    }

    window.onbeforeunload = function () {

        if (playersMap.contains(controlling) && playersMap.get(controlling).sort()[0] === myId) {
            playersMap.delete(controlling);
        }
        objectStore.clearPeersQueue();
        objectStore.clearServerQueue();
    };

    function init(gameWrapper) {
        wrapper = gameWrapper;

        var g_id = "";
        var parameters = location.search.substring(1).split("&");
        for (var i = 0; i < parameters.length; i++) {
            var temp = parameters[0].split("=");
            if (temp[0] === "gameid") {
                g_id = temp[1]
            }
        }
        if (g_id === "") {
            console.error("no game id in url");
            return;
        }

        var groupOptions = {
            id: g_id,
            secret: "default"
        };

        //Create legion instance
        legion = new Legion();

        //Callback for when there is an overlay change (e.g. a new user joins)
        legion.overlay.setOnChange(function (change, peerIds, serverIds) {
            var network = $("#network_status");
            var pc = $("#network_status_peerCount")[0];
            var sc = $("#network_status_server")[0];
            if (serverIds.length > 0) {
                sc.innerText = "Connected to [" + serverIds + "].";
            } else {
                sc.innerText = "Disconnected.";
            }
            if (peerIds.length == 0) {
                pc.innerText = "No peers.";
            } else {
                pc.innerText = "(" + peerIds.length + ") " + peerIds;
            }
            if (serverIds.length == 1) {
                network.removeClass("alert-success");
                network.addClass("alert-danger");
            } else {
                network.removeClass("alert-danger");
                network.addClass("alert-success");
            }
        });

        //Callback for when I join the overlay
        legion.joinGroup(groupOptions,
            function (group) {
                console.error("callback1");
                objectStore = group.getObjectStore();
                messageAPI = group.getMessageAPI();

            },
            function (server, data) {
                console.error("callback2");

                if (initDone)
                    return;

                if (data) {
                    OnGameFileLoaded();
                    initDone = true;
                } else if (server && !data) {
                    //Am first to join legion group (aka there is no data)
                    OnGameFileInitialized(); //Will create CRDTs with initial state
                    OnGameFileLoaded();
                    initDone = true;
                } else {
                    console.log("Waiting... server:" + server + " data:" + data);
                }
            },
            function (failMessage) {
                console.error("callback3");
                console.error("failed to join");
                console.error(failMessage)
            }
        );
    }

    //Initializes required crdts (state is detailed in file "models")
    function OnGameFileInitialized() {
        console.log("Initializing game state");

        //Requests the creation and fetch of a map crdt named "players".
        playersMap = objectStore.getOrCreate("players", legion.Map);


        gamestateMap = objectStore.getOrCreate("gamestate", legion.Map);
        //Modifies the state of the CRDT (in this case, sets the initial state)
        gamestateMap.set("state", STATE.WAITING);
        gamestateMap.set("prevstate", STATE.WAITING);

        gameMapInfoMap = objectStore.getOrCreate("gameMapInfo", legion.Map);
        gameMapInfoMap.set("height", INITVALUES.map.length);
        gameMapInfoMap.set("width", INITVALUES.map[0].length);

        gameMapList = objectStore.getOrCreate("gameMap", legion.Map);
        for (var i = 0; i < INITVALUES.map.length; i++) {
            for (var j = 0; j < INITVALUES.map[i].length; j++) {
                gameMapList.set(i * INITVALUES.map[0].length + j, INITVALUES.map[i][j]);
            }
        }

        sharedPacmanMap = objectStore.getOrCreate("pacmanState", legion.Map);
        sharedPacmanMap.set("position",
            {x: INITVALUES.pacman.position.x * 10, y: INITVALUES.pacman.position.y * 10});
        sharedPacmanMap.set("lives", INITVALUES.pacman.lives);
        sharedPacmanMap.set("score", INITVALUES.pacman.score);
        sharedPacmanMap.set("direction", INITVALUES.pacman.direction);
        sharedPacmanMap.set("due", INITVALUES.pacman.due);
        sharedPacmanMap.set("biscuitsEaten", INITVALUES.pacman.biscuitsEaten);


        for (i = 0; i < 4; i++) {
            sharedGhostsMap[i] = objectStore.getOrCreate("ghostState" + i, legion.Map);
            sharedGhostsMap[i].set("colour", INITVALUES.ghosts[i].colour);
            sharedGhostsMap[i].set('position',
                {x: INITVALUES.ghosts[i].position.x * 10, y: INITVALUES.ghosts[i].position.y * 10});
            sharedGhostsMap[i].set("direction", INITVALUES.ghosts[i].direction);
            sharedGhostsMap[i].set("due", INITVALUES.ghosts[i].due);
            sharedGhostsMap[i].set("eatable", false);
            sharedGhostsMap[i].set("eaten", false);
        }

        console.log("State Initialized");
    }

    //CRDTs have already been initialized by the first user to join
    function OnGameFileLoaded() {
        console.log("Seting up callbacks");

        //Fetch the CRDTs from the server
        playersMap = objectStore.getOrCreate("players", legion.Map);
        gamestateMap = objectStore.getOrCreate("gamestate", legion.Map);
        gameMapList = objectStore.getOrCreate("gameMap", legion.List);
        gameMapInfoMap = objectStore.getOrCreate("gameMapInfo", legion.Map);
        sharedPacmanMap = objectStore.getOrCreate("pacmanState", legion.Map);
        for (var i = 0; i < 4; i++) {
            sharedGhostsMap[i] = objectStore.getOrCreate("ghostState" + i, legion.Map);
        }

        //Setup handlers for when CRDTs are modified

        //Setup timestamp loggings for debugging
        playersMap.setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "playersMap")
        });
        gamestateMap.setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "gamestateMap")
        });
        gameMapList.setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "gameMapList")
        });
        gameMapInfoMap.setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "gameMapInfoMap")
        });
        sharedPacmanMap.setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "sharedPacmanMap")
        });
        sharedGhostsMap[0].setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "sharedGhostsMap0")
        });
        sharedGhostsMap[1].setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "sharedGhostsMap1")
        });
        sharedGhostsMap[2].setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "sharedGhostsMap2")
        });
        sharedGhostsMap[3].setOnStateChange(function (updates, meta) {
            logTimestamp(updates, "sharedGhostsMap3")
        });

        //Setup biscuit loggings
        gameMapList.setOnStateChange(logBiscuitDesync);

        gameMapList.setOnStateChange(gameMapChanged);
        gamestateMap.setOnStateChange(gameStateChanged);
        playersMap.setOnStateChange(playersMapChanged);


        //Setup local vars + wire
        var blockSizeY = (wrapper.offsetHeight - 30) / parseInt(gameMapInfoMap.get('height')[0]);
        var blockSizeX = wrapper.offsetWidth / parseInt(gameMapInfoMap.get('width')[0]);
        var blockSize = Math.min(blockSizeX, blockSizeY);

        map = new GameMap(gameMapList,
            parseInt(gameMapInfoMap.get('height')[0]),
            parseInt(gameMapInfoMap.get('width')[0]),
            blockSize);
        map.buildSpriteMap();

        localPacman = new Pac(map,
            JSON.parse(sharedPacmanMap.get('position')[0]), parseInt(sharedPacmanMap.get('lives')[0]),
            parseInt(sharedPacmanMap.get('score')[0]), parseInt(sharedPacmanMap.get('direction')[0]),
            parseInt(sharedPacmanMap.get('due')[0]), parseInt(sharedPacmanMap.get('biscuitsEaten')[0])
        );

        sharedPacmanMap.setOnStateChange(updatePacman);

        for (var z = 0; z < 4; z++) {
            localGhosts[z] = new Ghost(z,
                {
                    "isPacman": isPacman,
                    "getTick": getTick,
                    "isPlayerControlled": isPlayerControlled,
                    "secondsAgo": secondsAgo
                },
                map,
                sharedGhostsMap[z].get('colour')[0], JSON.parse(sharedGhostsMap[z].get('position')[0]),
                parseInt(sharedGhostsMap[z].get('direction')[0]), (sharedGhostsMap[z].get('eatable')[0] === "true"),
                (sharedGhostsMap[z].get('eaten')[0] === "true"), parseInt(sharedGhostsMap[z].get('due')[0])
            );
        }
        sharedGhostsMap[0].setOnStateChange(function (updates, meta) {
            updateGhost(0, updates);
        });
        sharedGhostsMap[1].setOnStateChange(function (updates, meta) {
            updateGhost(1, updates);
        });
        sharedGhostsMap[2].setOnStateChange(function (updates, meta) {
            updateGhost(2, updates);
        });
        sharedGhostsMap[3].setOnStateChange(function (updates, meta) {
            updateGhost(3, updates);
        });

        //Join players list
        chooseRole();

        console.log(myId);

        //Setup graphics
        var canvas = document.createElement("canvas");
        canvas.setAttribute("width", (blockSize * map.width) + "px");
        canvas.setAttribute("height", (blockSize * map.height) + 30 + "px");

        wrapper.style["min-width"] = (blockSize * map.width) + "px";
        wrapper.style["min-height"] = (blockSize * map.height) + 30 + "px";

        wrapper.appendChild(canvas);
        ctx = canvas.getContext('2d');

        //Start
        document.addEventListener("keydown", keyDown, true);
        document.addEventListener("keypress", keyPress, true);

        //Cheats
        $("#addLifeButton").click(function () {
            localPacman.lives += 1;
            sharedPacmanMap.set('lives', localPacman.lives);
        });
        $("#rerollButton").click(function () {
                var tempPosStore = [{}, {}, {}, {}];
                for (var k = 0; k < 4; k++) {
                    tempPosStore[k].position = localGhosts[(k + 1) % 4].position;
                    tempPosStore[k].direction = localGhosts[(k + 1) % 4].direction;
                    tempPosStore[k].due = localGhosts[(k + 1) % 4].due;
                }
                for (var z = 0; z < 4; z++) {
                    sharedGhostsMap[z].set('position',
                        {
                            x: Math.round(tempPosStore[z].position.x),
                            y: Math.round(tempPosStore[z].position.y)
                        });
                    sharedGhostsMap[z].set('direction', tempPosStore[z].direction);
                    sharedGhostsMap[z].set('due', tempPosStore[z].due);

                }
            }
        );


        $('#noCollisionCheck').change(function () {
            collisionEnabled = !$('#noCollisionCheck').is(':checked');
        });

        $('#soundCheck').change(function () {
            soundEnabled = $('#soundCheck').is(':checked');
        });

        console.log('setup completed, starting');
        timer = window.setInterval(mainLoop, 1000 / Pacman.FPS);
    }

    function gameMapChanged(updates, meta) {
        if (updates.set && (parseInt(updates.set.value[0]) == Pacman.EMPTY)) {
            if (parseInt(updates.was[0]) == Pacman.BISCUIT) {
                playSong("eating.short");
            }
            if (parseInt(updates.was[0]) == Pacman.BLUE_PILL) {
                playSong("eatpill");
            }
            if (parseInt(updates.was[0]) == Pacman.RED_PILL) {
                playSong("extra lives");
            }
        }
    }

    function playersMapChanged(updates, meta) {
        if (updates.set && updates.set.key == controlling) {
            console.log("Role messed with");
            if (playersMap.get(controlling).sort()[0] != myId) {
                console.log("Someone stole " + controlling + " from us!");
                chooseRole();
            }
        }
    }

    function chooseRole() {
        controlling = null;
        if (!playersMap.contains('pacman')) {
            playersMap.set('pacman', myId);
            controlling = 'pacman';
            console.log("Setting role to pacman");
        }
        if (controlling == null) {
            for (i = 0; i < 4; i++) {
                if (!playersMap.contains('ghost' + i)) {
                    playersMap.set('ghost' + i, myId);
                    console.log("Setting role to ghost" + i);
                    controlling = 'ghost' + i;
                    break;
                }
            }
        }
        if (controlling == null) {
            console.log('Setting role to spectator');
            controlling = 'spectator';
        }
    }

    function updateGhost(id, updates) {
        if (!updates.set)
            return;
        if (updates.set.key === 'colour') {
            localGhosts[id].colour = sharedGhostsMap[id].get('colour')[0];
        } else if (updates.set.key === 'position') {
            if (getState() === STATE.PLAYING) {
                var diff = {};
                diff.entity = "ghost" + id;
                diff.offsetX = localGhosts[id].position.x - JSON.parse(sharedGhostsMap[id].get('position')[0]).x;
                diff.offsetY = localGhosts[id].position.y - JSON.parse(sharedGhostsMap[id].get('position')[0]).y;
                diff.offsetTotal = Math.abs(diff.offsetX) + Math.abs(diff.offsetY);
                logMovementDesync(diff);
            }
            localGhosts[id].position = JSON.parse(sharedGhostsMap[id].get('position')[0]);
        } else if (updates.set.key === 'direction') {
            localGhosts[id].direction = parseInt(sharedGhostsMap[id].get('direction')[0]);
        } else if (updates.set.key === 'eatable') {
            localGhosts[id].eatable = (sharedGhostsMap[id].get('eatable')[0] === "true");
            if (localGhosts[id].eatable === true) {
                localGhosts[id].eatableStartTick = getTick();
            }
        } else if (updates.set.key === 'eaten') {
            localGhosts[id].eaten = (sharedGhostsMap[id].get('eaten')[0] === "true");
            if (localGhosts[id].eaten === true) {
                playSong("eatghost");
                localGhosts[id].eatenStartTick = getTick();
            }
        } else if (updates.set.key === 'due') {
            localGhosts[id].due = parseInt(sharedGhostsMap[id].get('due')[0]);
        }
    }

    function updatePacman(updates, meta) {
        if (!updates.set)
            return;
        if (updates.set.key === 'position') {
            if (getState() === STATE.PLAYING) {
                var diff = {};
                diff.entity = "pacman";
                diff.offsetX = localPacman.position.x - JSON.parse(sharedPacmanMap.get('position')[0]).x;
                diff.offsetY = localPacman.position.y - JSON.parse(sharedPacmanMap.get('position')[0]).y;
                diff.offsetTotal = Math.abs(diff.offsetX) + Math.abs(diff.offsetY);
                logMovementDesync(diff);
            }
            localPacman.position = JSON.parse(sharedPacmanMap.get('position')[0]);
        } else if (updates.set.key === 'lives') {
            localPacman.lives = parseInt(sharedPacmanMap.get('lives')[0]);
        } else if (updates.set.key === 'score') {
            localPacman.score = parseInt(sharedPacmanMap.get('score')[0]);
        } else if (updates.set.key === 'direction') {
            localPacman.direction = parseInt(sharedPacmanMap.get('direction')[0]);
        } else if (updates.set.key === 'due') {
            localPacman.due = parseInt(sharedPacmanMap.get('due')[0]);
        } else if (updates.set.key === 'biscuitsEaten') {
            localPacman.biscuitsEaten = parseInt(sharedPacmanMap.get('biscuitsEaten')[0]);
        }
    }

    function gameStateChanged(updates, meta) {
        if (updates.set.key === 'state') {
            var state = getState();
            switch (state) {
                case STATE.COUNTDOWN:
                    playSong("opening_song");
                    timerStart = tick;
                    break;
                case STATE.DYING:
                    playSong("die");
                    timerStart = tick;
                    break;
                case STATE.FINISHED:
                    playSong("intermission");
                    //TODO saveLogs
                    //saveLogs();
                    break;
            }
        }
    }

    function drawFooter() {

        var topLeft = (map.height * map.blockSize),
            textBase = topLeft + 22;

        //BG
        ctx.fillStyle = "#000000";
        ctx.fillRect(0, topLeft, (map.width * map.blockSize), 30);


        //AVATAR
        if (controlling == 'pacman') {
            ctx.fillStyle = "#FFFF00";
            ctx.beginPath();
            ctx.moveTo(10 + map.blockSize / 2, (topLeft + 6) + map.blockSize / 2);
            ctx.arc(10 + map.blockSize / 2, (topLeft + 6) + map.blockSize / 2,
                map.blockSize / 2, Math.PI * 0.25, Math.PI * 1.75, false);
            ctx.fill();
        } else if (controlling != 'spectator') {
            if (controlling == 'ghost0') {
                ctx.fillStyle = sharedGhostsMap[0].get("colour");
            } else if (controlling == 'ghost1') {
                ctx.fillStyle = sharedGhostsMap[1].get("colour");
            } else if (controlling == 'ghost2') {
                ctx.fillStyle = sharedGhostsMap[2].get("colour");
            } else if (controlling == 'ghost3') {
                ctx.fillStyle = sharedGhostsMap[3].get("colour");
            }
            var s = map.blockSize,
                top = topLeft + 5,
                left = 10;

            var tl = left + s;
            var base = top + s - 3;
            var inc = s / 10;

            var high = game.getTick() % 10 > 5 ? 3 : -3;
            var low = game.getTick() % 10 > 5 ? -3 : 3;

            ctx.beginPath();

            ctx.moveTo(left, base);

            ctx.quadraticCurveTo(left, top, left + (s / 2), top);
            ctx.quadraticCurveTo(left + s, top, left + s, base);

            // Wavy things at the bottom
            ctx.quadraticCurveTo(tl - (inc), base + high, tl - (inc * 2), base);
            ctx.quadraticCurveTo(tl - (inc * 3), base + low, tl - (inc * 4), base);
            ctx.quadraticCurveTo(tl - (inc * 5), base + high, tl - (inc * 6), base);
            ctx.quadraticCurveTo(tl - (inc * 7), base + low, tl - (inc * 8), base);
            ctx.quadraticCurveTo(tl - (inc * 9), base + high, tl - (inc * 10), base);

            ctx.closePath();
            ctx.fill();

            ctx.beginPath();
            ctx.fillStyle = "#FFF";
            ctx.arc(left + 6, top + 6, s / 6, 0, 300, false);
            ctx.arc((left + s) - 6, top + 6, s / 6, 0, 300, false);
            ctx.closePath();
            ctx.fill();

            var f = s / 12;
            var off = {};
            off[DIRECTIONS.RIGHT] = [f, 0];
            off[DIRECTIONS.LEFT] = [-f, 0];
            off[DIRECTIONS.UP] = [0, -f];
            off[DIRECTIONS.DOWN] = [0, f];

            ctx.beginPath();
            ctx.fillStyle = "#000";
            ctx.arc(left + 6 + off[DIRECTIONS.UP][0], top + 6 + off[DIRECTIONS.UP][1],
                s / 15, 0, 300, false);
            ctx.arc((left + s) - 6 + off[DIRECTIONS.UP][0], top + 6 + off[DIRECTIONS.UP][1],
                s / 15, 0, 300, false);
            ctx.closePath();
            ctx.fill();
        }

        //LIVES
        for (var i = 0, len = localPacman.lives; i < len; i++) {
            ctx.fillStyle = "#FFFF00";
            ctx.beginPath();
            ctx.moveTo(210 + (25 * i) + map.blockSize / 2, (topLeft + 6) + map.blockSize / 2);

            ctx.arc(210 + (25 * i) + map.blockSize / 2, (topLeft + 6) + map.blockSize / 2,
                map.blockSize / 2, Math.PI * 0.25, Math.PI * 1.75, false);
            ctx.fill();
        }

        /*
         ctx.fillStyle = !soundDisabled() ? "#00FF00" : "#FF0000";
         ctx.font = "bold 16px sans-serif";
         //ctx.fillText("♪", 10, textBase);
         ctx.fillText("s", 10, textBase);
         */

        //SCORE
        ctx.fillStyle = "#FFFF00";
        ctx.font = "14px BDCartoonShoutRegular";
        ctx.fillText("Score: " + localPacman.score, 65, textBase);
    }

    function draw() {
        var i, diff;
        map.draw(ctx);
        drawFooter();
        switch (getState()) {
            case STATE.PLAYING:
                for (i = 0; i < localGhosts.length; i += 1)
                    localGhosts[i].draw(ctx);
                localPacman.draw(ctx);
                break;
            case STATE.DYING:
                for (i = 0; i < localGhosts.length; i += 1)
                    localGhosts[i].draw(ctx);
                localPacman.drawDead(ctx, (tick - timerStart) / (Pacman.FPS * 2));
                break;
            case STATE.WAITING:
                dialog("Press N to start a New game");
                break;
            case STATE.PAUSE:
                for (i = 0; i < localGhosts.length; i += 1)
                    localGhosts[i].draw(ctx);
                localPacman.draw(ctx);
                dialog("Paused");
                break;
            case STATE.COUNTDOWN:
                diff = Math.floor(5 - secondsAgo(timerStart));
                dialog("Starting in: " + diff);
                break;
            case STATE.FINISHED:
                dialog("Game finished");
                break;
        }
    }

    function dialog(text) {
        ctx.fillStyle = "#FFFF00";
        ctx.font = "14px BDCartoonShoutRegular";
        var width = ctx.measureText(text).width,
            x = ((map.width * map.blockSize) - width) / 2;
        ctx.fillText(text, x, (map.height * 10) + 8);
    }

    function keyPress(e) {
        if (getState() !== STATE.WAITING && getState() !== STATE.PAUSE) {
            //e.preventDefault();
            //e.stopPropagation();
        }
    }

    function keyDown(e) {
        if (e.keyCode === KEY.N) {
            if (getState() == STATE.WAITING) {
                setState(STATE.COUNTDOWN);
            }
        } else if (e.keyCode === KEY.P) {
            if (getState() != STATE.PAUSE) {
                pause();
            } else {
                resume();
            }
        } else if (getState() !== STATE.PAUSE) {
            if (typeof keyMap[e.keyCode] !== "undefined") {
                if (controlling == 'pacman') {
                    sharedPacmanMap.set('due', keyMap[e.keyCode]);
                } else if (controlling == 'ghost0' && localGhosts[0].eaten == false) {
                    sharedGhostsMap[0].set('due', keyMap[e.keyCode]);
                } else if (controlling == 'ghost1' && localGhosts[1].eaten == false) {
                    sharedGhostsMap[1].set('due', keyMap[e.keyCode]);
                } else if (controlling == 'ghost2' && localGhosts[2].eaten == false) {
                    sharedGhostsMap[2].set('due', keyMap[e.keyCode]);
                } else if (controlling == 'ghost3' && localGhosts[3].eaten == false) {
                    sharedGhostsMap[3].set('due', keyMap[e.keyCode]);
                }
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        }
        return true;
    }

    function playSong(filename) {
        if (soundEnabled) {
            var audio = new Audio('audio/' + filename + '.mp3');
            audio.play();
        }
    }

    //LOGS
    function logBiscuitDesync(updates, meta) {
        return;
        //TODO update to legion
        //console.log(event);
        var index = event.index;
        var gridY = Math.floor(index / map.width);
        var gridX = index % map.width;
        var coordY = gridY * 10;
        var coordX = gridX * 10;
        diff = {}
        diff.offsetX = localPacman.position.x - coordX;
        diff.offsetY = localPacman.position.y - coordY;
        diff.direction = localPacman.direction;
        diff.desyncType = "none";
        if (diff.offsetX > 0) { //Direita
            if (localPacman.direction == DIRECTIONS.LEFT) {
                diff.desyncType = "behind";
            } else if (localPacman.direction == DIRECTIONS.RIGHT) {
                diff.desyncType = "ahead";
            }
        } else if (diff.offsetX < 0) { //Esquerda
            if (localPacman.direction == DIRECTIONS.LEFT) {
                diff.desyncType = "ahead";
            } else if (localPacman.direction == DIRECTIONS.RIGHT) {
                diff.desyncType = "behind"
            }
        }

        if (diff.offsetY < 0) { //Cima
            if (localPacman.direction == DIRECTIONS.UP) {
                diff.desyncType = "ahead";
            } else if (localPacman.direction == DIRECTIONS.DOWN) {
                diff.desyncType = "behind";
            }
        } else if (diff.offsetY > 0) { //Baixo
            if (localPacman.direction == DIRECTIONS.UP) {
                diff.desyncType = "behind";
            } else if (localPacman.direction == DIRECTIONS.DOWN) {
                diff.desyncType = "ahead";
            }
        }
        diff.timestamp = Date.now();
        biscuitLog.push(diff);
    }

    function logTimestamp(event, eventObject) {
        //TODO change to new format
        return;
        var obj = {}
        obj.timestamp = Date.now();
        obj.eventObject = eventObject;
        if (event.type == "value_changed") {
            obj.type = event.type;
            obj.property = event.property;
            obj.newValue = event.newValue;
            obj.oldValue = event.oldValue;
        } else if (event.type == "values_set") {
            obj.type = event.type;
            obj.index = event.index;
        }
        timestampLog.push(obj);
    }

    function logMovementDesync(diff) {
        diff.timestamp = Date.now();
        movementLog.push(diff);
    }

    function saveLogs() {
        saveTextAsFile("movementLog-" + controlling + "-" + Date.now() + ".log", JSON.stringify(movementLog));
        saveTextAsFile("biscuitLog-" + controlling + "-" + Date.now() + ".log", JSON.stringify(biscuitLog));
        saveTextAsFile("timestampLog-" + controlling + "-" + Date.now() + ".log", JSON.stringify(timestampLog));

    }

    function saveTextAsFile(title, text) {
        var textFileAsBlob = new Blob([text], {type: 'text/plain'});
        var downloadLink = document.createElement("a");
        downloadLink.download = title;
        downloadLink.innerHTML = "Download File";
        if (window.webkitURL != null) {
            // Chrome allows the link to be clicked
            // without actually adding it to the DOM.
            downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
        }
        else {
            // Firefox requires the link to be added to the DOM
            // before it can be clicked.
            downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
            //downloadLink.onclick = destroyClickedElement;
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);
        }

        downloadLink.click();
    }

    //END LOGS

    return {
        "init": init,
        "getTick": getTick,
        "isPacman": isPacman,
        "isPlayerControlled": isPlayerControlled
    };
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}